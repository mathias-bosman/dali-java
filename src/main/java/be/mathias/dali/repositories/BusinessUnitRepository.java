package be.mathias.dali.repositories;

import be.mathias.dali.models.BusinessUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BusinessUnitRepository extends JpaRepository<BusinessUnit, Integer> {
    @Query("SELECT bu FROM BusinessUnit bu WHERE bu.id = :buId")
    BusinessUnit find(@Param("buId") int buId);

    BusinessUnit findByName(String name);

    @Query("SELECT bu FROM BusinessUnit bu where bu.parentBusinessUnit.id = :parentId")
    List<BusinessUnit> findChildUnits(@Param("parentId") int parentId);
}
