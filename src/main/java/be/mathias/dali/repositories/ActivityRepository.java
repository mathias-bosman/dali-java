package be.mathias.dali.repositories;

import be.mathias.dali.models.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {
    @Query("SELECT a FROM Activity a WHERE a.id = :activityId")
    Activity find(@Param("activityId") int activityId);

    @Query("SELECT a FROM Activity a where a.parentActivity.id = :parentId")
    List<Activity> findChildActivities(@Param("parentId") int parentId);
}
