package be.mathias.dali.repositories;

import be.mathias.dali.models.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CompanyRepository extends JpaRepository<Company, Integer> {
    @Query("SELECT company FROM Company company WHERE company.id = :companyId")
    Company find(@Param("companyId") int companyId);

    Company findByName(String name);
}
