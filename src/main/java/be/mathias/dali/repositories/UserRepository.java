package be.mathias.dali.repositories;

import be.mathias.dali.models.BusinessUnit;
import be.mathias.dali.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("SELECT user FROM User user WHERE user.id = :userId")
    User find(@Param("userId") int userId);

    User findByUsername(String username);

    List<User> findAllByBusinessUnit(BusinessUnit businessUnit);

    @Query("SELECT user FROM User user WHERE user.active = true")
    List<User> getAllActiveUsers();
}
