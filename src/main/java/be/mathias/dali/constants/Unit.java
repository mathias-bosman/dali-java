package be.mathias.dali.constants;

public enum Unit {
    M_2("M²"),
    L_3("L³"),
    T("T");

    private String shorthand;

    Unit(String shorthand) {
        this.shorthand = shorthand;
    }

    public String getShorthand() {
        return shorthand;
    }
}
