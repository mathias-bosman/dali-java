package be.mathias.dali.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "business_units")
public class BusinessUnit implements DatabaseModel {
    private Integer id;
    private String name;
    private Company company;
    private BusinessUnit parentBusinessUnit;

    private List<User> users;

    @Id
    @Override
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BUSINESS_UNITS_SEQ")
    @SequenceGenerator(name = "BUSINESS_UNITS_SEQ", sequenceName = "SEQUENCE_BUSINESS_UNITS", allocationSize = 1)
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "fk_company", nullable = false)
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @ManyToOne
    @JoinColumn(name = "parent_unit")
    public BusinessUnit getParentBusinessUnit() {
        return parentBusinessUnit;
    }

    public void setParentBusinessUnit(BusinessUnit parentBusinessUnit) {
        this.parentBusinessUnit = parentBusinessUnit;
    }

    @OneToMany(mappedBy = "businessUnit")
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
