package be.mathias.dali.models;

public interface DatabaseModel {
    Integer getId();

    void setId(Integer id);
}
