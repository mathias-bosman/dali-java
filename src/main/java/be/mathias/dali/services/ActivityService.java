package be.mathias.dali.services;

import be.mathias.dali.constants.Unit;
import be.mathias.dali.models.Activity;
import be.mathias.dali.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class ActivityService implements ModelService<Activity> {
    private ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public Activity createActivity(String number, String description, Date validFrom, Date validUntil, Unit unit) {
        Activity activity = new Activity();
        activity.setNumber(number);
        activity.setDescription(description);
        activity.setValidFrom(validFrom);
        activity.setValidUntil(validUntil);
        activity.setUnit(unit);
        return activity;
    }

    @Transactional
    public Activity findActivity(int id) {
        return activityRepository.find(id);
    }

    @Transactional
    public List<Activity> findChildActivities(int parentId) {
        return activityRepository.findChildActivities(parentId);
    }

    @Override
    @Transactional
    public Activity save(Activity activity) {
        return activityRepository.save(activity);
    }

    @Override
    public boolean validate(Activity entity) {
        if (entity.getNumber() == null) {
            throw new IllegalStateException("Activity.number should not be null.");
        }

        if (entity.getValidFrom() == null) {
            throw new IllegalStateException("Acitity.validFrom should not be null.");
        }

        return true;
    }
}
