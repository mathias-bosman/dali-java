package be.mathias.dali.services;

import be.mathias.dali.models.Company;
import be.mathias.dali.repositories.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompanyService implements ModelService<Company> {

    private CompanyRepository companyRepository;

    @Autowired
    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public Company createCompany(String name) {
        Company company = new Company();
        company.setName(name);
        return company;
    }

    @Transactional
    public Company findCompany(String name) {
        return companyRepository.findByName(name);
    }

    @Transactional
    public Company findCompany(int id) {
        return companyRepository.find(id);
    }

    @Override
    public boolean validate(Company entity) {
        throw new UnsupportedOperationException("Validation of Company entitiy not yet implemnted.");
    }

    @Override
    @Transactional
    public Company save(Company entity) {
        return companyRepository.save(entity);
    }
}
