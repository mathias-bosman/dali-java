package be.mathias.dali.services;

import be.mathias.dali.models.DatabaseModel;

public interface ModelService<T extends DatabaseModel> {
    T save(T entity);

    boolean validate(T entity);
}
