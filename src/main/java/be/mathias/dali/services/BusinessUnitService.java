package be.mathias.dali.services;

import be.mathias.dali.models.BusinessUnit;
import be.mathias.dali.models.Company;
import be.mathias.dali.repositories.BusinessUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BusinessUnitService implements ModelService<BusinessUnit> {
    private BusinessUnitRepository businessUnitRepository;

    @Autowired
    public BusinessUnitService(BusinessUnitRepository businessUnitRepository) {
        this.businessUnitRepository = businessUnitRepository;
    }

    public BusinessUnit createBusinessUnit(String name, Company company) {
        return createBusinessUnit(name, null, company);
    }

    public BusinessUnit createBusinessUnit(String name, BusinessUnit parentBusinessUnit) {
        return createBusinessUnit(name, parentBusinessUnit, parentBusinessUnit.getCompany());
    }

    private BusinessUnit createBusinessUnit(String name, BusinessUnit parentBU, Company company) {
        BusinessUnit businessUnit = new BusinessUnit();
        businessUnit.setCompany(company);
        businessUnit.setParentBusinessUnit(parentBU);
        businessUnit.setName(name);
        return businessUnit;
    }

    @Transactional
    public BusinessUnit findBusinessUnit(String name) {
        return businessUnitRepository.findByName(name);
    }

    @Transactional
    public BusinessUnit findBusinessUnit(int id) {
        return businessUnitRepository.find(id);
    }

    @Transactional
    public List<BusinessUnit> findChildUnits(int parentId) {
        return businessUnitRepository.findChildUnits(parentId);
    }

    @Override
    @Transactional
    public BusinessUnit save(BusinessUnit businessUnit) {
        return businessUnitRepository.save(businessUnit);
    }

    @Override
    public boolean validate(BusinessUnit entity) {
        BusinessUnit parentBusinessUnit = entity.getParentBusinessUnit();
        if (parentBusinessUnit != null) {
            if (!parentBusinessUnit.getCompany().equals(entity.getCompany())) {
                throw new IllegalStateException("The company of the parent business unit (" + parentBusinessUnit.getCompany().getId() + ")" +
                        "does not match the child unit's company (" + entity.getCompany() + ")");
            }
        }

        return true;
    }
}
