package be.mathias.dali.services;

import be.mathias.dali.models.BusinessUnit;
import be.mathias.dali.models.User;
import be.mathias.dali.repositories.UserRepository;
import be.mathias.dali.utilities.PasswordUtility;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService implements ModelService<User> {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(String username, String emailAddress, String password, BusinessUnit businessUnit) {
        User user = new User();
        user.setUsername(username);
        user.setEmailAddress(emailAddress);
        Pair<byte[], byte[]> saltHashPair = PasswordUtility.createSaltHashPair(password);
        user.setPasswordSalt(saltHashPair.getLeft());
        user.setPasswordHash(saltHashPair.getRight());
        user.setBusinessUnit(businessUnit);
        user.setActive(true);
        return user;
    }

    @Transactional
    public User findUser(String username) {
        return userRepository.findByUsername(username);
    }

    @Transactional
    public User findUser(int id) {
        return userRepository.find(id);
    }

    public List<User> findUsersByBusinessUnit(BusinessUnit businessUnit) {
        return userRepository.findAllByBusinessUnit(businessUnit);
    }

    @Transactional
    public void deactivateUser(int id) {
        User user = findUser(id);
        user.setActive(false);
    }

    @Override
    public boolean validate(User entity) {
        throw new UnsupportedOperationException("Validation of User entitiy not yet implemnted.");
    }

    @Override
    @Transactional
    public User save(User entity) {
        return userRepository.save(entity);
    }
}
