-- auto-generated definition
create sequence sequence_users;
create table users
(
  id               integer default nextval('sequence_users') not null
    constraint users_pkey
      primary key,
  username         varchar(10)                               not null,
  active           boolean default false                     not null,
  passwordsalt     bytea,
  passwordhash     bytea,
  emailaddress     varchar(256),
  fk_business_unit integer
    constraint users_business_units_id_fk
      references business_units,
  firstname        varchar(255),
  lastname         varchar(255)
);

create unique index users_id_uindex
  on users (id);

create unique index users_username_uindex
  on users (username);

create unique index users_emailaddress_uindex
  on users (emailaddress);

