-- auto-generated definition
create sequence sequence_business_units;
create table business_units
(
  id          integer default nextval('sequence_business_units') not null
    constraint business_units_pk
      primary key,
  name        varchar(255)                                       not null,
  fk_company  integer                                            not null
    constraint business_units_companies_id_fk
      references companies,
  parent_unit integer
    constraint business_units_business_units_id_fk
      references business_units
);