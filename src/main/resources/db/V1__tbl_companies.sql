-- auto-generated definition
create sequence sequence_companies;
create table companies
(
  id   integer default nextval('sequence_companies') not null
    constraint companies_pk
      primary key,
  name varchar(255)                                  not null
);