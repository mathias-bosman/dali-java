create sequence sequence_activities;
create table activities
(
  id              integer default nextval('sequence_activities') not null
    constraint activities_pk
      primary key,
  listnumber      varchar(50)                                    not null,
  description     varchar(255)                                   not null,
  validFrom       date                                           not null,
  validUntil      date,
  unit            varchar(10),
  parent_activity integer
    constraint activities_activities_id_fk
      references activities
);