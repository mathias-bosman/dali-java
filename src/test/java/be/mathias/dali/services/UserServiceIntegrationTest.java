package be.mathias.dali.services;

import be.mathias.dali.models.BusinessUnit;
import be.mathias.dali.models.Company;
import be.mathias.dali.models.User;
import be.mathias.dali.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.PersistenceException;
import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class UserServiceIntegrationTest extends ServiceIntegrationTest {

    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TransactionTemplate txTemplate;

    @BeforeEach
    void before() {
        this.userService = new UserService(userRepository);
    }

    private Company createCompany() {
        Company company = new Company();
        company.setName("Comp");
        return create(company);
    }

    private BusinessUnit createTestBU(Company company) {
        BusinessUnit businessUnit = new BusinessUnit();
        businessUnit.setName("BU");
        businessUnit.setCompany(company);
        return create(businessUnit);
    }

    private User createTestUser() {
        return userService.createUser("bob", "bob@company.com", "password", createTestBU(createCompany()));
    }

    @Test
    void save() {
        User user = createTestUser();
        store(user);
        assertNotNull(user.getId());
    }

    @Test
    @Transactional(propagation = Propagation.NEVER)
    void createUser() {
        assertThrows(PersistenceException.class, () -> store(userService.createUser(null, "bob@company.com", "password", createTestBU(createCompany()))));
        assertThrows(PersistenceException.class, () -> store(userService.createUser("user", "bob@company.com", "password", null)));
        assertThrows(DataIntegrityViolationException.class,
                () -> txTemplate.execute(new TransactionCallbackWithoutResult() {
                    @Override
                    protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                        Company comp = createCompany();
                        store(userService.createUser("bob", "mail", "pass", createTestBU(comp)));
                        store(userService.createUser("bob", "mail2", "pass", createTestBU(comp)));
                    }
                }));
    }

    @Test
    void findUser() {
        store(createTestUser());
        assertNotNull(userService.findUser("bob"));
        assertNull(userService.findUser("max"));
    }

    @Test
    void deactivateUser() {
        User user = userService.createUser("bob", "mail", "pass", createTestBU(createCompany()));
        store(user);
        assertTrue(user.isActive());
        userService.deactivateUser(user.getId());
        assertFalse(user.isActive());
    }

    @Test
    void findUsersByBusinessUnit() {
        BusinessUnit businessUnit = createTestBU(createCompany());
        store(userService.createUser("bob", "mail", "pass", businessUnit));
        store(userService.createUser("max", "mail2", "pass", businessUnit));
        store(userService.createUser("tom", "mail3", "pass", businessUnit));

        List<User> usersByBusinessUnit = userService.findUsersByBusinessUnit(businessUnit);
        assertNotNull(usersByBusinessUnit);
        assertThat(usersByBusinessUnit, hasSize(3));
    }
}