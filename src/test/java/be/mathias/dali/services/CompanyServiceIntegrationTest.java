package be.mathias.dali.services;

import be.mathias.dali.models.Company;
import be.mathias.dali.repositories.CompanyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;

class CompanyServiceIntegrationTest extends ServiceIntegrationTest {
    private CompanyService companyService;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private TransactionTemplate txTemplate;

    @BeforeEach
    void before() {
        this.companyService = new CompanyService(companyRepository);
    }

    @Test
    void createCompany() {
        Company company = companyService.createCompany("Google");
        store(company);
        assertNotNull(company.getId());
    }

    @Test
    @Transactional(propagation = Propagation.NEVER)
    void createInvalidCompany() {
        assertThrows(PersistenceException.class,
                () -> store(companyService.createCompany(null)));
        assertThrows(DataIntegrityViolationException.class,
                () -> txTemplate.execute(new TransactionCallbackWithoutResult() {
                    @Override
                    protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                        store(companyService.createCompany("Google"));
                        store(companyService.createCompany("Google"));
                    }
                }));
    }

    @Test
    void findCompany() {
        Company company = companyService.createCompany("Google");
        store(company);
        assertNotNull(companyService.findCompany("Google"));
        assertNotNull(companyService.findCompany(company.getId()));
    }
}
