package be.mathias.dali.services;

import be.mathias.dali.constants.Unit;
import be.mathias.dali.models.Activity;
import be.mathias.dali.repositories.ActivityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class ActivityServiceIntegrationTest extends ServiceIntegrationTest {

    private ActivityService activityService;
    @Autowired
    private ActivityRepository activityRepository;

    @BeforeEach
    void before() {
        this.activityService = new ActivityService(activityRepository);
    }

    private Activity testActivity(String number, String description, Date validFrom, Date validUntil, Unit unit) {
        return activityService.createActivity(number, description, validFrom, validUntil, unit);
    }

    @Test
    void createActivity() {
        Activity activity = testActivity("1.2", "activity description", new Date(), new Date(), Unit.T);
        store(activity);
        assertNotNull(activity.getId());
    }

    @Test
    @Transactional
    void findActivity() {
        Activity activity = testActivity("1.2", "activity description", new Date(), new Date(), Unit.T);
        store(activity);
        assertNotNull(activityService.findActivity(activity.getId()));
    }

    @Test
    void findChildActivities() {
        Activity parent = testActivity("1", "activity description", new Date(), new Date(), Unit.T);
        store(parent);
        Activity child1 = testActivity("1.1", "activity description", new Date(), new Date(), Unit.T);
        child1.setParentActivity(parent);
        store(child1);
        Activity child2 = testActivity("1.2", "activity description", new Date(), new Date(), Unit.T);
        child2.setParentActivity(parent);
        store(child2);
        List<Activity> childActivities = activityService.findChildActivities(parent.getId());
        assertThat(childActivities, hasSize(2));
    }

    @Test
    void save() {
        // Tested in other methods
    }

    @Test
    void validate() {
        Activity activityInvalidNumber = testActivity(null, "valid descriptin", new Date(), new Date(), Unit.M_2);
        Activity activityInvalidValidFrom = testActivity("1.1", "valid descriptin", null, new Date(), Unit.M_2);

        assertThrows(IllegalStateException.class, () -> activityService.validate(activityInvalidNumber), "Activity.number should not be null.");
        assertThrows(IllegalStateException.class, () -> activityService.validate(activityInvalidValidFrom), "Acitity.validFrom should not be null.");
    }
}