package be.mathias.dali.services;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:test.properties")
public abstract class ServiceIntegrationTest {
    @PersistenceContext
    private EntityManager entityManager;

    void store(Object entity) {
        entityManager.persist(entity);
    }

    void store(Object... entities) {
        for (Object entity : entities) {
            store(entity);
        }
    }

    <T> T create(T entity) {
        entityManager.persist(entity);
        return entity;
    }

    <E> int count(Class<E> entityClass) {
        return number("select count(entity) from " + entityClass.getSimpleName() + " entity");
    }

    private int number(String query) {
        return entityManager.createQuery(query, Number.class).getSingleResult().intValue();
    }
}
