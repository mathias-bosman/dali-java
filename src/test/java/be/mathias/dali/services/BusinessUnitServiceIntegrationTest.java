package be.mathias.dali.services;

import be.mathias.dali.models.BusinessUnit;
import be.mathias.dali.models.Company;
import be.mathias.dali.repositories.BusinessUnitRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class BusinessUnitServiceIntegrationTest extends ServiceIntegrationTest {
    private BusinessUnitService businessUnitService;
    @Autowired
    private BusinessUnitRepository businessUnitRepository;

    @BeforeEach
    void before() {
        this.businessUnitService = new BusinessUnitService(businessUnitRepository);
    }

    private BusinessUnit createTestBusinessUnit(String name, Company company) {
        return businessUnitService.createBusinessUnit(name, company);
    }

    @Test
    void createBusinessUnit() {
        Company company = new Company();
        company.setName("Google");
        store(company);
        BusinessUnit businessUnit = createTestBusinessUnit("Logistics", company);
        store(businessUnit);
        assertNotNull(businessUnit.getId());

        BusinessUnit childBU = businessUnitService.createBusinessUnit("Stationary", businessUnit);
        store(childBU);
        assertNotNull(childBU.getId());
        assertEquals(childBU.getCompany(), businessUnit.getCompany());
    }

    @Test
    void findBusinessUnit() {
        Company company = new Company();
        company.setName("Google");
        store(company);
        BusinessUnit businessUnit = createTestBusinessUnit("Logistics", company);
        store(businessUnit);
        assertNotNull(businessUnitService.findBusinessUnit("Logistics"));
        assertNotNull(businessUnitService.findBusinessUnit(businessUnit.getId()));
    }

    @Test
    void findChildUnits() {
        Company company = new Company();
        company.setName("Google");
        store(company);
        BusinessUnit parentUnit = createTestBusinessUnit("Logistics", company);
        BusinessUnit childUnit1 = createTestBusinessUnit("Paper", company);
        BusinessUnit childUnit2 = createTestBusinessUnit("Paperclips", company);
        childUnit1.setParentBusinessUnit(parentUnit);
        childUnit2.setParentBusinessUnit(parentUnit);
        store(parentUnit, childUnit1, childUnit2);
        assertThat(businessUnitService.findChildUnits(parentUnit.getId()), hasSize(2));
    }

    @Test
    void validate() {
        Company company = new Company();
        company.setName("Google");
        Company company2 = new Company();
        company.setName("Comp 2");

        BusinessUnit parentBU = businessUnitService.createBusinessUnit("Logistics", company);
        assertTrue(businessUnitService.validate(parentBU));
        BusinessUnit childBU = businessUnitService.createBusinessUnit("Stationary", company2);
        childBU.setParentBusinessUnit(parentBU);
        assertThrows(IllegalStateException.class, () -> businessUnitService.validate(childBU));
        //TODO: don't just check class of exception - in time this won't be enough
    }
}