package be.mathias.dali.services.utilities;

import be.mathias.dali.utilities.PasswordUtility;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordUtilityTest {
    @Test
    void passWordHash() {
        String password = "thereIsNoSpoon";
        Pair<byte[], byte[]> saltHashPair = PasswordUtility.createSaltHashPair(password);
        byte[] salt = saltHashPair.getLeft();
        byte[] hash = saltHashPair.getRight();
        assertTrue(PasswordUtility.isExpectedPassword(password, salt, hash));
        assertFalse(PasswordUtility.isExpectedPassword("itIsAFork", salt, hash));
    }
}