package be.mathias.dali.repositories;

import be.mathias.dali.models.Activity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

class ActivityRepositoryTest extends RepositoryTest {
    @Autowired
    private ActivityRepository activityRepository;

    private Activity createTestActivity(String number, String description, Date validFrom) {
        Activity activity = new Activity();
        activity.setNumber(number);
        activity.setDescription(description);
        activity.setValidFrom(validFrom);
        return activity;
    }

    @Test
    void findChildActivities() {
        Activity parentActivity = createTestActivity("1", "parent activity", new Date());
        store(parentActivity);
        Activity childActivity = createTestActivity("1.1", "child activity 1", new Date());
        childActivity.setParentActivity(parentActivity);
        store(childActivity);
        Activity childActivity2 = createTestActivity("1.2", "child activity 2", new Date());
        childActivity2.setParentActivity(parentActivity);
        store(childActivity2);

        List<Activity> childActivities = activityRepository.findChildActivities(parentActivity.getId());
        assertThat(childActivities, hasSize(2));
    }
}
