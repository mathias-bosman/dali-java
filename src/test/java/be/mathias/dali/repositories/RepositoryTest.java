package be.mathias.dali.repositories;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:test.properties")
public abstract class RepositoryTest {
    @PersistenceContext
    private EntityManager entityManager;

    void store(Object entity) {
        entityManager.persist(entity);
    }

    <T> T create(T entity) {
        entityManager.persist(entity);
        return entity;
    }
}
