package be.mathias.dali.repositories;

import be.mathias.dali.models.BusinessUnit;
import be.mathias.dali.models.Company;
import be.mathias.dali.models.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class UserRepositoryTest extends RepositoryTest {

    @Autowired
    UserRepository repository;

    @Test
    void findByUsername() {
        User user = new User();
        user.setUsername("bob");
        Company company = new Company();
        company.setName("Comp.");
        store(company);
        BusinessUnit businessUnit = new BusinessUnit();
        businessUnit.setName("BU");
        businessUnit.setCompany(company);
        store(businessUnit);
        user.setBusinessUnit(businessUnit);
        store(user);

        assertNotNull(repository.findByUsername("bob"));
        assertNull(repository.findByUsername("jef"));
    }

    @Test
    void getAllActiveUsers() {
        Company company = new Company();
        company.setName("Comp.");
        store(company);
        BusinessUnit businessUnit = new BusinessUnit();
        businessUnit.setName("BU");
        businessUnit.setCompany(company);
        store(businessUnit);

        User user = new User();
        user.setUsername("bob");
        user.setBusinessUnit(businessUnit);
        user.setActive(true);
        store(user);
        User user2 = new User();
        user2.setUsername("jef");
        user2.setBusinessUnit(businessUnit);
        user2.setActive(false);
        store(user2);

        assertThat(repository.getAllActiveUsers(), hasSize(1));
    }
}