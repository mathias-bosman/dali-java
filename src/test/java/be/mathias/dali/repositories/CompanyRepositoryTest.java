package be.mathias.dali.repositories;

import be.mathias.dali.models.Company;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class CompanyRepositoryTest extends RepositoryTest {
    @Autowired
    CompanyRepository companyRepository;

    @Test
    void findByName() {
        Company company = new Company();
        company.setName("Google");
        store(company);
        assertNotNull(companyRepository.findByName("Google"));
        assertNull(companyRepository.findByName("Bogus"));
    }
}
