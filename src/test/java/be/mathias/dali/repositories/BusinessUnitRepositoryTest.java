package be.mathias.dali.repositories;

import be.mathias.dali.models.BusinessUnit;
import be.mathias.dali.models.Company;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class BusinessUnitRepositoryTest extends RepositoryTest {
    @Autowired
    private BusinessUnitRepository businessUnitRepository;

    private BusinessUnit createTestBusinessUnit(String name, Company company) {
        BusinessUnit businessUnit = new BusinessUnit();
        businessUnit.setName(name);
        businessUnit.setCompany(company);
        return create(businessUnit);
    }

    @Test
    void findByName() {
        Company company = new Company();
        company.setName("Google");
        createTestBusinessUnit("Logistics", create(company));
        assertNotNull(businessUnitRepository.findByName("Logistics"));
        assertNull(businessUnitRepository.findByName("Bogus"));
    }

    @Test
    void findChildBusinessUnits() {
        Company company = new Company();
        company.setName("Google");
        store(company);
        BusinessUnit parentUnit = createTestBusinessUnit("Parent unit", company);
        BusinessUnit childUnit1 = createTestBusinessUnit("Child unit 1", company);
        BusinessUnit childUnit2 = createTestBusinessUnit("Child unit 2", company);
        childUnit1.setParentBusinessUnit(parentUnit);
        childUnit2.setParentBusinessUnit(parentUnit);
        List<BusinessUnit> childUnits = businessUnitRepository.findChildUnits(parentUnit.getId());
        assertThat(childUnits, hasSize(2));
    }
}
